import { AppCommunicationInterface } from "./app-communication.interface";
import { CommunicationDataType } from "@timerocket/assistant-data-model";

export interface AppCommunicationResponseInterface<
  T extends CommunicationDataType | void = void
> extends AppCommunicationInterface {
  data?: T;
  user: {
    id: 1;
  };
}
