import { AppCommunicationReplyInterface } from "./app-communication-reply.interface";
import { AppCommunicationResponseInterface } from "./app-communication-response.interface";
import { CommunicationDataType } from "@timerocket/assistant-data-model";

export declare type CommunicationType =
  | AppCommunicationReplyInterface
  | AppCommunicationResponseInterface<CommunicationDataType>;
