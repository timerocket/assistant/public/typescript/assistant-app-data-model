import { AppProfileSectionItemTypeEnum } from "./app-profile-section-item.type.enum";
import { AppIconSuffixEnum } from "../app-icon-suffix.enum";
import {
  AppNavigationComponentDataType,
  AppNavigationNameEnum,
} from "../navigation";
import { AppProfileSectionItemCategoryEnum } from "./app-profile-section-item.category.enum";
import { AppIconEnum } from "../app-icon.enum";

export interface AppProfileSectionItemInterface {
  type: AppProfileSectionItemTypeEnum;
  category: AppProfileSectionItemCategoryEnum;
  title: string;
  iconSuffix: AppIconSuffixEnum;
  icon: AppIconEnum;
  navigationName?: AppNavigationNameEnum;
  data?: AppNavigationComponentDataType;
}
