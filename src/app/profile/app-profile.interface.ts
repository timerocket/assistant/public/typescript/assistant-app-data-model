import { AppProfileEmailInterface } from "./app-profile-email.interface";
import { AppProfileSectionInterface } from "./app-profile-section.interface";

export interface AppProfileInterface {
  id: string;
  firstName: string;
  lastName: string;
  avatar: string;
  emails: AppProfileEmailInterface[];
  sections: AppProfileSectionInterface[];
}
