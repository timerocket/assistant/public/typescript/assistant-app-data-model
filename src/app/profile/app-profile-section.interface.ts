import { AppProfileSectionTypeEnum } from "./app-profile-section.type.enum";
import { AppProfileSectionItemInterface } from "./app-profile-section-item.interface";

export interface AppProfileSectionInterface {
  type: AppProfileSectionTypeEnum;
  title: string;
  items: AppProfileSectionItemInterface[];
}
