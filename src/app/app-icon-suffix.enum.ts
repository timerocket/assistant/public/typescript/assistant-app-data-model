/**
 * @deprecated
 */
export enum AppIconSuffixEnum {
  PRICE_TAG = "pricetag",
  DOCUMENT = "document",
}
