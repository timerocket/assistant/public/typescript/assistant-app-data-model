import { CommunicationType } from "./communication.type";
import { AppNavigationInterface } from "./navigation";
import { AppProfileInterface } from "./profile";
import { CardType, CardInterface } from "@timerocket/assistant-data-model";

export interface AppDataInterface {
  /**
   * @deprecated Use communications property instead
   */
  messages: CommunicationType[];
  communications: CommunicationType[];
  navigation: AppNavigationInterface[];
  profile: AppProfileInterface;
  cards: CardInterface<CardType>[];
}
