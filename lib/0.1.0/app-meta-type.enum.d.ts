export declare enum AppMetaTypeEnum {
    APP_MESSAGE_NEW_REPLY = "app.message.new-reply",
    APP_CARD_SAVED = "app.card.saved",
    APP_DATA = "app.data"
}
