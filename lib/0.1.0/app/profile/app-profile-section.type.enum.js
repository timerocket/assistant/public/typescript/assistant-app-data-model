"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppProfileSectionTypeEnum = void 0;
var AppProfileSectionTypeEnum;
(function (AppProfileSectionTypeEnum) {
    AppProfileSectionTypeEnum["USER_INFORMATION"] = "user.profile.section.user-information";
})(AppProfileSectionTypeEnum = exports.AppProfileSectionTypeEnum || (exports.AppProfileSectionTypeEnum = {}));
