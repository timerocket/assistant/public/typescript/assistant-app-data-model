"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppProfileSectionItemCategoryEnum = void 0;
var AppProfileSectionItemCategoryEnum;
(function (AppProfileSectionItemCategoryEnum) {
    AppProfileSectionItemCategoryEnum["INSURANCE_MEDICAL"] = "insurance.medical";
})(AppProfileSectionItemCategoryEnum = exports.AppProfileSectionItemCategoryEnum || (exports.AppProfileSectionItemCategoryEnum = {}));
