export declare enum AppProfileSectionItemTypeEnum {
    INSURANCE_CARD = "user.profile.section.user-information.item.insurance-card",
    TEXT = "user.profile.section.user-information.item.text"
}
