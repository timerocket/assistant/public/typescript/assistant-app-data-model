import { CommunicationTypeEnum } from "@timerocket/assistant-data-model";
export interface AppCommunicationInterface {
    id: string;
    text: string;
    type: CommunicationTypeEnum;
    user: {
        id: 1 | 2;
    };
    createdAt: string;
}
