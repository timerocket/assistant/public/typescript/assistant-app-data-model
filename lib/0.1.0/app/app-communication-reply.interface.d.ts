import { AppCommunicationInterface } from "./app-communication.interface";
export interface AppCommunicationReplyInterface extends AppCommunicationInterface {
    user: {
        id: 2;
    };
}
