"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppIconSuffixEnum = void 0;
var AppIconSuffixEnum;
(function (AppIconSuffixEnum) {
    AppIconSuffixEnum["PRICE_TAG"] = "pricetag";
    AppIconSuffixEnum["DOCUMENT"] = "document";
})(AppIconSuffixEnum = exports.AppIconSuffixEnum || (exports.AppIconSuffixEnum = {}));
