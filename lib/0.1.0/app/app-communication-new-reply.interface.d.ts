import { AppCommunicationInterface } from "./app-communication.interface";
import { AppDevice } from "@timerocket/assistant-data-model";
export interface AppCommunicationNewReplyInterface extends AppCommunicationInterface {
    device: AppDevice;
    user: {
        id: 2;
    };
}
