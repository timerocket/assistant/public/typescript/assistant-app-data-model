import { CommunicationType } from "./communication.type";
import { AppNavigationInterface } from "./navigation";
import { AppProfileInterface } from "./profile";
import { CardType, CardInterface } from "@timerocket/assistant-data-model";
export interface AppDataInterface {
    messages: CommunicationType[];
    communications: CommunicationType[];
    navigation: AppNavigationInterface[];
    profile: AppProfileInterface;
    cards: CardInterface<CardType>[];
}
