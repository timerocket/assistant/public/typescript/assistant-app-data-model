"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppMetaTypeEnum = void 0;
var AppMetaTypeEnum;
(function (AppMetaTypeEnum) {
    AppMetaTypeEnum["APP_MESSAGE_NEW_REPLY"] = "app.message.new-reply";
    AppMetaTypeEnum["APP_CARD_SAVED"] = "app.card.saved";
    AppMetaTypeEnum["APP_DATA"] = "app.data";
})(AppMetaTypeEnum = exports.AppMetaTypeEnum || (exports.AppMetaTypeEnum = {}));
