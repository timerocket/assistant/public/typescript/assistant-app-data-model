# Data Models
Shared data models for Assistant applications for both front end and backend

## In scope
- Plain interfaces and classes that *could* be used by both front end and backend application

## Out of scope
- Interfaces or classes that *can* only be used by a front end or backend application
- Dependencies